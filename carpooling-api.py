from tinydb import TinyDB, Query
import random
import falcon
import json
from faker import Faker

fake = Faker('es_MX')
db = TinyDB('db.json')

intereses_posibles = ["App Page", "appliances", "Baby Goods/Kids Goods", "Bags/Luggage", "Board Game", "Brand",
                      "Building Materials", "Camera/Photo", "Cars", "Clothing (Brand)", "Commercial Equipment",
                      "Computers (Brand)", "Electronics", "Food & Beverage Company", "Furniture", "Games/Toys",
                      "Health/Beauty", "Home Décor", "Household Supplies", "Jewelry/Watches", "Kitchen/Cooking",
                      "Office Supplies", "Patio/Garden", "Pet Supplies", "Pharmaceuticals", "Phone/Tablet",
                      "Product/Service", "Software", "Tools/Equipment", "Video Game", "Vitamins/Supplements",
                      "Website", "Wine/Spirits"]


def get_payload(req):
    payload = req.stream.read()
    payload = json.loads(payload)
    return payload


def exists_publi_ad(id):
    db = TinyDB('db.json')
    return db.contains(doc_ids=[id])


class PublicAds(object):
    def is_valid_publiad(self, payload):
        params = set(['client_id', 'title', 'text', 'url', 'ubication'])
        received_params = set(payload.keys())
        obligatory_keys = params - received_params
        if len(obligatory_keys) == 0:
            return True, []
        else:
            return False, list(obligatory_keys)

    def on_post(self, req, res):
        print('LLego una peticion post a publi-ads')
        payload = get_payload(req)
        is_valid, keys_with_problems = self.is_valid_publiad(payload)
        if not is_valid:
            body = {'error': 'El anuncio debe tener los siguientes campos %s' % str(keys_with_problems)}
            res.body = json.dumps(body)
            res.status = falcon.HTTP_400
        else:
            payload['type'] = 'publi-ad'
            id_public_ad = db.insert(payload)
            body = {'id': id_public_ad}
            res.body = json.dumps(body)
            res.status = falcon.HTTP_201


class Demanda(object):
    def is_valid_ubication(self, params):
        expected_params = set(['lat', 'long'])
        obligatory_params = expected_params - set(params.keys())
        if len(obligatory_params) == 0:
            return True, []
        else:
            return False, list(obligatory_params)

    def on_get(self, req, res):
        params = req.params
        is_valid, necessary_params = self.is_valid_ubication(params)
        if not is_valid:
            body = {'error': 'La peticion debe tener los siguientes parametros %s' % str(necessary_params)}
            res.body = json.dumps(body)
            res.status = falcon.HTTP_400
        else:
            all_ubications = []
            for i in range(1, random.randint(2, 6)):
                all_ubications.append({
                    'latitude': float(params['lat']) + random.uniform(0.01, 0.2),
                    'longitude': float(params['long']) + random.uniform(0.01, 0.2),
                    'range': random.randint(100, 1000),
                    'possible_clients': random.randint(5, 18)
                })
            res.body = json.dumps(all_ubications)
            res.status = falcon.HTTP_200


class InteresesPorZona(object):
    def is_valid_ubication(self, params):
        expected_params = set(['lat', 'long'])
        obligatory_params = expected_params - set(params.keys())
        if len(obligatory_params) == 0:
            return True, []
        else:
            return False, list(obligatory_params)

    def on_get(self, req, res):
        params = req.params
        is_valid, necessary_params = self.is_valid_ubication(params)
        if not is_valid:
            body = {'error': 'La peticion debe tener los siguientes parametros %s' % str(necessary_params)}
            res.body = json.dumps(body)
            res.status = falcon.HTTP_400
        else:
            intereses = []
            for interes in random.sample(intereses_posibles, random.randint(1, 14)):
                intereses.append({
                    'interes': interes,
                    'clientes': random.randint(5, 50)
                })
            res.body = json.dumps(intereses)
            res.status = falcon.HTTP_200


class RendimientoAnuncio(object):
    def on_get(self, req, res):
        params = req.params
        id_anuncio = int(params['id_ad'])
        if exists_publi_ad(id_anuncio):
            body = []
            for i in range(1, random.randint(2, 10)):
                name = fake.name()
                body.append({
                    'conductor': name,
                    'conductor_id': ''.join(map(lambda s: s[0], name.split(' '))) + str(random.randint(1, 1000)),
                    'impresiones': random.randint(1, 100)
                })
            status = falcon.HTTP_200
        else:
            body = {'error': 'El codigo {} de publi-ad no se encuentra'.format(id_anuncio)}
            status = falcon.HTTP_404
        res.body = json.dumps(body)
        res.status = status


class Conversiones(object):
    sedes = ['SAN DIEGO', 'LAURELES', 'BELEN', 'CENTRO', 'SABANETA', 'ENVIGADO', 'POBLADO', 'MOLINOS']

    def is_valid_conversion(self, payload):
        params = set(['qr_code', 'referencia_compra', 'sede_compra'])
        received_params = set(payload.keys())
        obligatory_keys = params - received_params
        if len(obligatory_keys) == 0:
            return True, []
        else:
            return False, list(obligatory_keys)

    def on_get(self, req, res):
        id_anuncio = int(req.params['id_ad'])
        if exists_publi_ad(id_anuncio):
            body = []
            for i in range(1, random.randint(2, 10)):
                name = fake.name()
                body.append({
                    'conductor': name,
                    'conductor_id': ''.join(map(lambda s: s[0], name.split(' '))) + str(random.randint(1000, 9999)),
                    'referencia_compra': fake.ssn(),
                    'sede_compra': random.choice(self.sedes),
                    'valor': round(random.random() * random.randint(10000, 100000), 3)
                })
            status = falcon.HTTP_200
        else:
            body = {'error': 'El codigo {} de publi-ad no se encuentra'.format(id_anuncio)}
            status = falcon.HTTP_404
        res.body = json.dumps(body)
        res.status = status

    def on_post(self, req, res):
        payload = get_payload(req)
        is_valid, keys_with_problems = self.is_valid_conversion(payload)
        if not is_valid:
            body = {'error': 'La conversion debe tener los siguientes campos %s' % str(keys_with_problems)}
            res.body = json.dumps(body)
            res.status = falcon.HTTP_400
        else:
            res.body = json.dumps(payload)
            res.status = falcon.HTTP_201


app = falcon.API()
public_ads = PublicAds()
demanda = Demanda()
rendimiento = RendimientoAnuncio()
conversiones = Conversiones()
intereses = InteresesPorZona()
app.add_route('/external-api/publi-ads/', public_ads)
app.add_route('/external-api/conversiones/', conversiones)
app.add_route('/external-api/rendimiento-ad/', rendimiento)
app.add_route('/external-api/consultar-demanda/', demanda)
app.add_route('/external-api/intereses-ruta/', intereses)
